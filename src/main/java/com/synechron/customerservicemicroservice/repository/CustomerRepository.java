
  package com.synechron.customerservicemicroservice.repository;
  
  import org.springframework.data.jpa.repository.JpaRepository; import
  org.springframework.stereotype.Repository;

import com.synechron.customerservicemicroservice.model.Customer;
  
  
  @Repository public interface CustomerRepository extends JpaRepository<Customer, Long>{
  
  // Customer getAccountById(Long id);
  
  }
 